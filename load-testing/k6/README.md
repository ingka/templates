# K6 Load / Performance / Stress Testing

[[_TOC_]]

## Description

K6 https://k6.io/ is a load testing tool with excellent tooling and support for anything you can do with javascript!

It can be used to check for performance regression in Merge Requests, load testing with 1000's of users, or even to stress test your application to breaking point.

You can set the job to fail based on all kinds of metrics, the most common of which is response time and/or error rate - but don't let your imagination stop there

Its a very well documented tool - find more documentation on how to write your tests scripts at https://k6.io/docs/ but your test can be as simple as:

```javascript
import { check, group } from 'k6';
import http from 'k6/http';
import { Rate } from 'k6/metrics';

// Test configuration
export const errorRate = new Rate('errors');
export const options = {
  stages: [
    { duration: '30s', target: 15 }, // Ramp up to target # users over duration
    { duration: '30s', target: 15 }, // Stay at target # users for duration
    { duration: '30s', target: 0 }, // Ramp down to target # users over duration
  ],
  thresholds: {
    http_req_duration: ['p(95)<20001'], // 95th percentile response time < X milliseconds
    errors: ['rate<0.01'], // Maximum % of the requests are allowed to fail where 100%=1.0
  },
  ext: {
    loadimpact: {
      name: 'test.loadimpact.com',
    },
  },
};

// User scenarios
export default function () {
  group('Home then search by item number', () => {
    const resProducts = http.get(`${__ENV.BASE_URL}/products`);
    const resSearch = http.get(`${__ENV.BASE_URL}/products?page=1&size=20&sort=itemName,asc&q=70163357`);
    // Make sure the status code is 200 OK
    const resultProducts = check(resProducts, {
      'is status 200': r => r.status === 200,
    });
    const resultSearch = check(resSearch, {
      'is status 200': r => r.status === 200,
    });
    errorRate.add(!resultProducts);
    errorRate.add(!resultSearch);
  });
}

```

## Usage

### Available Variables

| Name | Default Value | Notes |
| ---  | ---           | --- |
| K6_BASE_URL | `"http://$CI_PROJECT_NAME.$KUBE_NAMESPACE.svc.cluster.local"` | MANDATORY! Provide the base URL for the environment you are testing |
| K6_TEST_FILE | `"$CI_PROJECT_DIR/cicd/load-test/main.js"` | The path to your test script |

### Default Usage 

The default case is simply to use the template as it is - providing only the environment's base URL for tests and the test script

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/load-testing/k6/load-test.yml'
load-test:
  variables:
    K6_BASE_URL: "http://mybranch.mysite.com"
    K6_TEST_FILE: "$CI_PROJECT_DIR/cicd/load-test/main.js"
```

### Reuse for an Optional Stress Test Job

The following reuses the job to add a manually started Stress Test version of your K6 script which can be executed in any branch, but not the production pipeline.

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/load-testing/k6/load-test.yml'

load-test:
  variables:
    K6_BASE_URL: "http://mybranch.mysite.com"
    K6_TEST_FILE: "$CI_PROJECT_DIR/cicd/load-test/main.js"

stress-test:
  extends: load-test
  variables:
    K6_BASE_URL: "http://mybranch.mysite.com"
    K6_TEST_FILE: "$CI_PROJECT_DIR/cicd/load-test/stress-test.js"
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_BRANCH
      when: manual
      allow_failure: true
```
