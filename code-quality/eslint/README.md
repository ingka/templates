# ESLINT

## Description

ESLint is a popular and widely used linting / code quality tool for Javascript / Node applications. 

The job will run either a default command or your own execution of ESLint and break the build if it fails. Right now it will upload an the HTML report but soon it will also integrate with Gitlab's code quality UI too.

See https://eslint.org for more information

## Usage

### Available Variables

| Name | Default Value | Notes |
| ---  | ---           | --- |
| ES_WORK_DIR | `$CI_PROJECT_DIR` | Path from which eslint command should run |
| ES_CONFIG_PATH | `$CI_PROJECT_DIR/.eslintrc.js` | Path to your eslint configuration file |
| ES_IGNORE_PATH | `$CI_PROJECT_DIR/.eslintignore` | Path to your eslint ignore file|
| ES_EXT | `".js,.vue"` | Comma seperated list of file extensions to scan |
| ES_COMMAND | `""` | Override the default ESLint command with your own |
| ES_HTML_REPORT | `""` | Path to HTML output to keep as artifact |

### Default Usage 

If you do not already have a script definition and want to run a default version of the job - simply use the template as it is. This will run the following command using your application image built in an earlier job.

`npx eslint "$ES_WORK_DIR" --config "$ES_CONFIG_PATH" --ignore-path "$ES_IGNORE_PATH" --ext "$ES_EXT" --color -f node_modules/eslint-html-reporter/reporter.js -o $CI_PROJECT_DIR/eslint-report.html"`

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/code-quality/eslint/eslint.yml'
```

### Custom Usage

#### Use your own ESLint script

It is more likely that you already have a configured node script or similar to run ESLint. Here is an example of how to configure in such a case:

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/code-quality/eslint/eslint.yml'
variables:
  ES_WORK_DIR: "/home/node"
  ES_COMMAND: "yarn run lint:ci"
  ES_HTML_REPORT: "/home/node/eslint-report.html"
```

#### Use a different image for the job

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/code-quality/eslint/eslint.yml'

eslint:
  image: node14:latest
  variables:
    ES_WORK_DIR: "/home/node"
    ES_COMMAND: "yarn run lint:ci"
    ES_HTML_REPORT: "/home/node/eslint-report.html"
```