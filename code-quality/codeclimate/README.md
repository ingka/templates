# CODE CLIMATE

[[_TOC_]]

## Description

Uses Code Climate https://codeclimate.com/quality/ Engines, which are free and open source. Code Quality does not require a Code Climate subscription. Helps identify common programming issues such as repetitive code, unused code, methods that are too complex etc and supports most languages.

**Gitlab's own template** will produce the results and a json report that is used for integration with the UI (Reports, MR integration, stats etc). If you would also want an HTML version you need trigger a parallel job for the HTML report (example below)

**The custom template in this repository** Will allow you to "break the build" based on your own criteria. Plus it produces both the json artifact required for Gitlab UI integration and an HTML version that is human readable in a single job.

## Gitlab's Own Default Template

### JSON report only
```yml
include:
  - template: Code-Quality.gitlab-ci.yml
```
See https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html for more configuration options

### Parallel job to get an HTML version too
```yml
# Provides the default JSON report
include:
  - template: Code-Quality.gitlab-ci.yml
# A parallel job to get an HTML version
code_quality_html:
  extends: code_quality
  variables:
    REPORT_FORMAT: html
  artifacts:
    paths: [gl-code-quality-report.html]
```

## Using the Custom Template

### Available Variables

| Name | Default Value | Notes |
| ---  | ---           | --- |
| CC_CONF | `".codeclimate.yml"` | Full path to your .codeclimate.yml if it is not at the root of the project |
| CC_BLOCKERS_ALLOWED | `"0"` | How many "Blocker" issues are allowed before job fails |
| CC_CRITICAL_ALLOWED | `"0"` | How many "Critical" issues are allowed before job fails |
| CC_MAJOR_ALLOWED | `"0"` | How many "Major" issues are allowed before job fails |
| CC_MINOR_ALLOWED | `"5"` | How many "Minor" issues are allowed before job fails |
| CC_INFO_ALLOWED | `"10"` | How many "Info" issues are allowed before job fails |

### Default Usage 

The default case (if you have configured your tests to work like the default variables) is simply to use the template as it is.

```yaml
# Produces both the JSON and HTML reports in a single job plus ability to break build
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/code-quality/codeclimate/code-quality.yml'
```

### Custom Usage

#### If your configuration is not at root and adjust when the job fails
```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/code-quality/codeclimate/code-quality.yml'
code-quality:
  variables:
    CC_CONF: "config/.codeclimate.yml"
    CC_BLOCKERS_ALLOWED: "1"
    CC_CRITICAL_ALLOWED: "2"
    CC_MAJOR_ALLOWED: "3"
    CC_MINOR_ALLOWED: "10"
    CC_INFO_ALLOWED: "20"
```
