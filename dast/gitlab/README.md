# GITLAB DAST (ZAPROXY) SCANNER

[[_TOC_]]

## Description

An adapation of Gitlab's DAST scanning job (https://docs.gitlab.com/ee/user/application_security/dast/) which in turn is an implementation of OWASP's ZAProxy (https://www.zaproxy.org/)

This job will scan your website/API for the OWASP top 10 vulnerabilitis such as XSS and Cookie issues.

## Optional Configurations

For a Complete list of all available variables see https://docs.gitlab.com/ee/user/application_security/dast/#available-variables

There are variables fo configuring all kinds of edge cases, providing API specifications, Spidering and the ZA Proxy itself.

## Usage

This single template should be able to handle sites/APIs that have no authentication, basic authentication or use Azure AD authentication.

**Note**: If your service requires Azure AD authentication you must specify `AZURE_AD_AUTH: "true"` and provide the CICD Variables for the `CLIENT_ID` and `CLIENT_SECRET` - see the example below

### Public (No authentication) 

Include the template along with ONE of DAST_PATHS or DAST_PATHS_FILE - you cannot specify both.

`DAST_PATHS` would be a comma seperated list of paths to test. If you have many paths it may be simpler to provide a file instead with one path on each line as `DAST_PATHS_FILE`

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/dast/gitlab/dast.yml'
dast:
  variables:
    DAST_WEBSITE: https://mybranch.mydomain.com
    DAST_PATHS: "/index.html,/user/login.html,/products"
```

or 

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/dast/gitlab/dast.yml'
dast:
  variables:
    DAST_WEBSITE: https://mybranch.mydomain.com
    DAST_PATHS_FILE: "my_paths.txt"
```

### Basic Auth

Include the template along with the address of the deployed version of your site, a comma seperated list of paths to test, plus the CICD Variables holding user/password for accessing the application. You may also need to add the specific names of the fields in the form in which the credentials should be added.

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/dast/gitlab/dast.yml'
dast:
  variables:
    DAST_WEBSITE: https://mybranch.mydomain.com
    DAST_PATHS: "/index.html,/user/login.html,/products"
    DAST_USERNAME: $USERNAME_CICD_VARIABLE
    DAST_PASSWORD: $PASSWORD_CICD_VARIABLE
    DAST_USERNAME_FIELD: session[user] 
    DAST_PASSWORD_FIELD: session[password]
```

### Azure AD Authentication

Include the template along with the address of the deployed version of your site, a comma seperated list of paths to test, plus the CICD Variables holding the Client ID and Client Secret for getting an auth token. This token will be added to an auth header on all requests.

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/dast/gitlab/dast.yml'
dast:
  variables:
    DAST_WEBSITE: https://mybranch.mydomain.com
    DAST_PATHS: "/index.html,/user/login.html,/products"
    AZURE_AD_AUTH: "true"
    CLIENT_ID: $AZURE_CLIENT_ID_CICD_VARIABLE
    CLIENT_SECRET: $AZURE_CLIENT_SECRET_CICD_VARIABLE
```
