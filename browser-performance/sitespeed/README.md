# SITESPEED.IO BROWSER TESTING

[[_TOC_]]

## Description

Sitespeed is an excellent browser-performance testing tool that will test your pages against a variety of benchmarks and your own performance budget. As well as detailed metrics such as those listed at https://www.sitespeed.io/documentation/sitespeed.io/metrics/, and security metrics it produces videos, HAL's and more as artifacts plus of course HTML reports. You can add your own performance budget for all metrics plus, with the paid version of Gitlab, integrate performance data with Merge requests and other parts of the Gitlab UI.

The supported browsers to use for testing are:
- Firefox
- Safari
- Edge
- Chrome
- Chrome on Android
- Firefox on Android
- Safari on iOS

**Note:** If you want to test multiple browsers its a good idea to run parallel tests per browser for speed in the pipeline! See example below.

## Available Variables

| Name | Default Value | Notes |
| ---  | ---           | --- |
| SS_BASE_URL | `"http://$CI_PROJECT_NAME.$KUBE_NAMESPACE.svc.cluster.local"` | MANDATORY! Provide the base URL of the environment to test |
| SS_URL_FILE | `$CI_PROJECT_DIR/cicd/browser-test/urls.txt` | MANDATORY! Provide a list of paths that should be test in a file, one path per line |
| SS_PRESURF_URL | `$SS_BASE_URL` | A pre-compilation path to hit before tests start |
| SS_PLUGINS | `"./gitlab-exporter"` | A comma seperated list of plugins to include |
| SS_OUTPUT_FOLDER | `"$CI_PROJECT_DIR/sitespeed-results"` | Where all reports, videos etc will be stored |
| SS_BROWSER | `"chrome"` | The browser to use for the test |
| SS_BUDGET_FILE | `"$CI_PROJECT_DIR/cicd/browser-test/perf-budget.json"` | The tag of the image destination |
| SS_ITERATIONS | `"5"` | CICD Variable containing the username to log into the destination registry |

## Usage

### Default Usage

The default usage would require you to provide both the SS_BASE_URL and a file containing the paths to tests on that URL. This would perform 5 iterations using chrome against all the paths in the SS_URL_FILE.

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/browser-performance/sitespeed/browser-performance.yml'
browser-performance:
  variables:
    SS_BASE_URL: "http://featurebranch.mysite.com"
    SS_URL_FILE: "$CI_PROJECT_DIR/cicd/browser-test/urls.txt"
```

### Custom Usage

#### Use another browser

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/browser-performance/sitespeed/browser-performance.yml'
browser-performance:
  variables:
    SS_BASE_URL: "http://featurebranch.mysite.com"
    SS_URL_FILE: "$CI_PROJECT_DIR/cicd/browser-test/urls.txt"
    SS_BROWSER: "firefox"
```

#### Parallel jobs per browser

We would extend the template to multiple jobs:

```yaml

include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/browser-performance/sitespeed/browser-performance.yml'
# Job 1
browser-performance:
  variables:
    SS_BASE_URL: "http://featurebranch.mysite.com"
    SS_URL_FILE: "$CI_PROJECT_DIR/cicd/browser-test/urls.txt"
    SS_BROWSER: "chrome"
# Job 2
browser-performance-firefox:
  extends: browser-performance
  variables:
    SS_BASE_URL: "http://featurebranch.mysite.com"
    SS_URL_FILE: "$CI_PROJECT_DIR/cicd/browser-test/urls.txt"
    SS_BROWSER: "firefox"
# Job 3
browser-performance-edge:
  extends: browser-performance
  variables:
    SS_BASE_URL: "http://featurebranch.mysite.com"
    SS_URL_FILE: "$CI_PROJECT_DIR/cicd/browser-test/urls.txt"
    SS_BROWSER: "edge"
```