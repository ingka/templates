# GITLAB SAST

[[_TOC_]]

## Description

Gitlab's own SAST template is quite comprehensive! It is a single job that detects languages used and even the presence of K8s manifests and more - then starts automatically the right tools for the language.

Amongst the ever growing list is :

| Tool | Language |
| ---  | ---      |
| Security Code Scan | .NET |
| PMD | Salesforce / Apex |
| Flawfinder | C / C++ |
| SoBelow | Elixr / Pheonix |
| Gosec | Golang |
| FindSecBugs | Java / Groovy / Scala |
| Kubesec | Helm charts / Kubernetes Manifests |
| MobSF | Java Android / Kotlin Andoid / Objective C (iOS) / Swift (iOS) |
| ESLint Security Plugin | Javascript / TypeScript |
| ESLint React Plugin | React |
| NodeJsScan | Node |
| phpcs-security-audit | PHP |
| Bandit | Python |
| Brakeman | Ruby |

## Usage

The template is not provided here - just use Gitlab's own:

```yaml
include:
  - template: Security/SAST.gitlab-ci.yml
```

This will automatically create all the relevant jobs according to what is found in your project!

## Customisation General

First see the extensive documentation at https://docs.gitlab.com/ee/user/application_security/sast/ for a full picture of what you can configure at a general level.

To enable and disable various tools in the suite plus some high level configurations there are a large number of variables you can use. (Specifically this section https://docs.gitlab.com/ee/user/application_security/sast/#available-variables)

For example you can configure Gosec to only fail on Medium or higher vulnerabilities you could use:

```yaml
include:
  - template: Security/SAST.gitlab-ci.yml

variables:
  SAST_GOSEC_LEVEL: 2
```

Likewise you can control which tools are allowed to run by providing a list:

```yaml
include:
  - template: Security/SAST.gitlab-ci.yml

variables:
  SAST_DEFAULT_ANALYZERS: eslint, kubesec, nodejs-scan, spotbugs
```

## Customisation Fine-grained

### General

All the individual tools rely on their usual methods to configure them. For example .eslintrc at the root of the project to configure ESLint. Just visit the relevant tool's documentation to configure in a very fine-grained way.

### Java

Java (Maven, Gradle, etc projects) in particular can be fun to configure! Pay attention especially to the variables:

`GRADLE_PATH`, `JAVA_OPTS`, `JAVA_PATH`, `SAST_JAVA_VERSION`, `MAVEN_CLI_OPTS`, `MAVEN_PATH`, `MAVEN_REPO_PATH`

### Kubesec

#### Kubernetes Manifests

You must specifically enable the scanning of Kubernetes Manifest files

```
include:
  - template: Security/SAST.gitlab-ci.yml

variables:
  SCAN_KUBERNETES_MANIFESTS: "true"
```

#### Helm charts

Note the information in the docs about dependency builds in the documenation https://docs.gitlab.com/ee/user/application_security/sast/#analyzer-settings. Additionally you may need to use `KUBESEC_HELM_CHARTS_PATH` and `KUBESEC_HELM_OPTIONS` depending on the location and config of your charts