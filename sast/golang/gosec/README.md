# Gosec(SAST for Golang)

[[_TOC_]]

## Description

Gosec https://github.com/securego/gosec is a tool for finding unsafe code in Go applications. If your project is a pure Go project and speed of pipeline is paramount, then this job can be useful instead of the Gitlab SAST job. It will be faster and skip a lot steps not required otherwise.

## Usage

### Available Variables

| Name | Default Value | Notes |
| ---  | ---           | --- |
| GOSEC_CONF | `"$CI_PROJECT_DIR/gosec-config.json"` | Change if your config is under another path |

### Default Usage 

The default case is simply to use the template as it is - just specify the path to the already built jar file that you wish to scan.

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/sast/golang/gosec/sast-gosec.yml'
```

### Custom Usage

Most configuration is done via config file. If you want to change the path to file in your project use:

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/sast/golang/gosec/sast-gosec.yml'
sast-gosec:
  variables:
    GOSEC_CONF: "/completely/different/config.json"
```
