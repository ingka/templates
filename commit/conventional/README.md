# SEMANTIC RELEASE COMMIT LINT

## Description

A simple job that lints commits against the conventional config https://github.com/conventional-changelog/commitlint/tree/master/@commitlint/config-conventional#rules

Semantic Release workflows depend on this commit convention to decide when to release, create CHANGELOG.md and / or release notes.  By default it runs in all branches except master.

## Usage

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/commit/conventional/commit-lint.yml'
```
