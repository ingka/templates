# SEMANTIC RELEASE

[[_TOC_]]

## Description

Semantic Release is a manually triggered job (see below for example on how to switch to automatic) which performs a semantic release based on a semantic commit git history. For more information on what semantic-release is see https://github.com/semantic-release/semantic-release

The image used for this job includes the following packages/plugins:

- `semantic-release`
- `@semantic-release/gitlab`
- `@semantic-release/git`
- `@semantic-release/changelog`
- `@semantic-release/exec`

## Usage

### Pre-Requisites

- Create a PAT for a service account with API scope & save it as a CICD variable
- Makes sure to specify the variable as the `GITLAB_TOKEN` in the configuration below

### Available Variables

| Name | Default Value | Notes |
| ---  | ---           | --- |
| GITLAB_TOKEN | `$SEM_GL_TOKEN` | MANDATORY! The CICD variable holding the authentication token |

### Default Usage 

The default case is simply to use the template as it is - providing only the CI/CD Variable that contains your access token for Gitlab

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/release/semantic-release/semantic-release.yml'
semantic-release:
  variables:
    GITLAB_TOKEN: $SEMRELEASE_TOKEN_CICD_VARIABLE
```

### Custom Usage

#### Trigger semantic release on every push to master

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/testing/cypress/integration-tests.yml'
semantic-release:
  variables:
    GITLAB_TOKEN: $SEMRELEASE_TOKEN_CICD_VARIABLE
  rules:
    - if: $CI_COMMIT_REF == "master"
      when: on_success
```
