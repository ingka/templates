# PUBLISH RELEASE NOTES BASED ON MERGED MR's TO SLACK WEBHOOK

[[_TOC_]]

## THIS IS NOT A TEMPLATE (yet)!

Release notes are an incredibly personal thing to the team and I am trying to think of a good way to do this one as a template.

Teams can argue for days about what should / shouldn't be included, how it should look, what channels etc - so for now I am leaving this as an example whilst I put my thinking cap on.

Feel free to copy / paste into your project and adapt how you like before including it with something like

```
include:
  - local: 'path/to/the/release-notes.yml'
```

## Basics of how it works

### Pre-requisites

* Expects two CICD Variables to exist, `MR_TOKEN` and `$SLACK_RELEASE_WEBHOOK_CUSTOMERS`
* `MR_TOKEN` should contain a Gitlab access token with API scope
* `$SLACK_RELEASE_WEBHOOK_CUSTOMERS` is the Slack Webhook to fire the release notes at

### Basic Overview

* gets project tags from Gitlab API
* finds the latest and previous tags
* uses npm package "git-release-notes" to parse git log and find MR's between the two tags
* passes the json through a couple of EJS templates for Slack version and an HTML version
* POSTs the slack version at the webhook
* saves the HTML version as a job artifact

