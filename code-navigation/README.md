# CODE-NAVIGATION

[[_TOC_]]

## Description

A very optional job that adds code navigation features common to IDE's into various places of the Gitlab UI. It essentially extracts an LSIF dump of your code to utilise throughout various views.

See https://docs.gitlab.com/ee/user/project/code_intelligence.html for more information about this feature

## Usage

Just include the relevant template:

### Golang

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/code-navigation/golang.yml'
```

### Java

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/code-navigation/java.yml'
```

### NodeJS

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/code-navigation/nodejs.yml'
```