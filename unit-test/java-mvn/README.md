# UNIT TEST (Java + Maven)

[[_TOC_]]

## Description

This unit test job will:

- run your tests against a Java / Maven application
- get code coverage results
- integrate both the results and the coverage into the Gitlab UI for statistics, Merge Requests and other areas too.

For now, this job runs using an OpenJDK11 container by default, but an example of how to use an other Java image as the base is found below.

## Pre-requistites

### Expects

- Unit test results are output using Surefire reports to $MAVEN_TARGET_DIR/surefire-reports/TEST-*.xml
- Unit test results are output in junit format

### Add Jacoco to your POM for Code Coverage

For Code Coverage to work you will need to add the Jacoco plugin to your POM.

Example:
```xml
...
    <build>
     ...
        <plugins>
         ...
            <!--For providing test coverage reports -->
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>0.8.6</version>
                <executions>
                    <execution>
                        <id>pre-unit-test</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                        <configuration>
                            <destFile>${project.build.directory}/jacoco/jacoco.exec</destFile>
                        </configuration>
                    </execution>
                    <execution>
                        <id>report</id>
                        <phase>test</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                        <configuration>
                            <dataFile>${project.build.directory}/jacoco/jacoco.exec</dataFile>
                            <outputDirectory>${project.build.directory}/jacoco/reports</outputDirectory>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        ...
        </plugins>
    ...
    </build>
...

```

## Usage

### Available Variables

| Name | Default Value | Notes |
| ---  | ---           | --- |
| MAVEN_SETTINGS | `$MAVEN_SETTINGS` | Optional: a CICD Variable holding the contents of a Maven Settings file (e.g. to auth with a maven repo) |
| MAVEN_WRAPPER_PATH | `"$CI_PROJECT_DIR/mvnw"` | Full path to your Maven Wrapper script |
| MAVEN_TARGET_DIR | `"target"` | Relative path to directory your applications are buit in |
| MAVEN_OPTS | `"-Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn -B"` | Pass any arguments you may need |

### Default Usage 

The default case (if you have configured your tests to work like the default variables) is simply to use the template as it is.

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/unit-test/java-mvn/unit-test.yml'
```

### Custom Usage

#### With a Maven Settings file

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/unit-test/java-mvn/unit-test.yml'
unit-tests:
  variables:
    MAVEN_SETTINGS: $MY_CICD_VARIABLE_HOLDING_THE_CONTENTS
```

#### Using a different Java image

Override the template's settings using normal Gitlab CI syntax e.g:

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/unit-test/java-mvn/unit-tests.yml'
# Override Template
unit-tests:
  image: openjdk:8-alpine
```