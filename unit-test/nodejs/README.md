# UNIT TEST (Node.js)

[[_TOC_]]

## Description

This unit test job will:

- run your tests against a Node.js application
- get code coverage results
- integrate both the results and the coverage into the Gitlab UI for statistics, Merge Requests and other areas too.

It utilises your already built container to perform the tests. If this is not possible then an example of how to use another/generic container image can be found below.

Bare in mind it is difficult to make this a completely generic job. Anything configurable (your test command, where your reports are output to etc) are exposed as variables - but you may need to adapt your current test package to fit - or simply use this template as a reference to create your own job.

But if you have any suggestions - let us know!

## Pre-requistites / Assumptions

The following assumptions are made in order to integrate properly with the Gitlab UI. They are optional and the job will still provide a pass/fail if you don't adhere to them but you will be missing out on some very nice integrations!

- Unit test results are transformed into junit XML format
- Code Coverage is reported in Covertura XML format
- Total Code coverage can be extracted by the regex `'/All files[^|]*\|[^|]*\s+([\d\.]+)/'`

## Usage

### Available Variables

| Name | Default Value | Notes |
| ---  | ---           | --- |
| UT_WORK_DIR | `"/home/node"` | The location from which your unit test scripts should run |
| UT_CMD | `"yarn run test:unit-ci"` | The command that should run to perform your tests |
| UT_JUNIT_REPORT_XML | `"/home/node/junit.xml"` | The path where the tests will output a JUnit-formatted XML report |
| UT_COVERAGE_REPORT_XML | `"/home/node/coverage/cobertura-coverage.xml"` | The path where the tests will output a Covertura-formatted XML report |
| UT_HTML_REPORT | `"/home/node/test-report.html"` | The path where the tests will output a Covertura-formatted XML report |

### Default Usage 

The default case (if you have configured your tests to work like the default variables) is simply to use the template as it is.

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/unit-test/nodejs/unit-tests.yml'
```

### Custom Usage

#### Use your own command, working directory plus differently named reports

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/unit-test/nodejs/unit-tests.yml'
unit-tests:
  variables:
    UT_WORK_DIR: "/app/"
    UT_CMD: "npm run test:unit-ci"
    UT_JUNIT_REPORT_XML: "/app/reports/junit.xml"
    UT_COVERAGE_REPORT_XML: "/app/reports/coverage.xml"
    UT_HTML_REPORT: "/app/reports/report.html"
```

#### Use a different / generic container to perform the tests

Override the template's settings using normal Gitlab CI syntax e.g:

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/unit-test/nodejs/unit-tests.yml'
# Override Template
unit-tests:
  image: node:14-alpine
```
