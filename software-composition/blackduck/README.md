# BLACKDUCK SOFTWARE COMPOSITION ANALYSIS

[[_TOC_]]

## DESCRIPTION

Synopsis BlackDuck https://www.blackducksoftware.com/ software composition analysis will check your dependencies for vulnerabilities and when used in this job you can optionally break your build in order to "detect early" and "fix early" vulnerabilities in the packages that your application depends on.

Blackduck covers the majority of languages and package managers. This job will by default check every push to every branch and fail if a package is found to be in violation of a policy you have configured in Blackduck.

## Usage

### Prerequisites
Ingka Teams have a list of [mandatory prerequisites](https://confluence.build.ingka.ikea.com/display/SSEC/Mandatory+Prerequisites+-+Ingka+Teams) that needs to be followed in order to comply with our internal standards and help the Security Team with their automated scans.  
Please follow the linked guide to decide the group and project names, create an AD Service Account and add your groups there before you start with this template.

### Available Variables

| Name | Default Value | Notes |
| ---  | ---           | --- |
| BD_API_KEY | `$BLACKDUCK_API_KEY` | MANDATORY! Provide the CICD Variable that holds your API Key |
| BD_PROJECT_PREFIX | `null` | MANDATORY! Is the agreed Group Name and is prefixed to $BD_PROJECT_NAME |
| BD_PROJECT_NAME | `$CI_PROJECT_NAME` | |
| BD_VERSION_NAME | `$CI_COMMIT_REF_NAME` | |
| BD_SEARCH_DEPTH | `3` | |
| BD_CLEANUP | `false` | |
| BD_WAIT_FOR_RESULTS | `true` | |
| BD_TIMEOUT | `1200` | |
| BD_SOURCE_PATH | `$CI_PROJECT_DIR` | |
| BD_CREATE_PDF_REPORT | `"true"` | |
| BD_CREATE_NOTICES_REPORT | `"false"` | |
| BD_EXTRA_FLAGS | `null` | experimental! used to pass any other detect.sh flags to Blackduck |

### Default Usage 

The default case is simply to use the template as it is - providing only your API Key (provisioned in the Blackduck portal) as a secret variable plus a Prefix to be prepended to your project's name

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/software-composition/blackduck/sca-blackduck.yml'
sca-blackduck:
  variables:
    BD_API_KEY: $BLACKDUCK_API_KEY
    BD_PROJECT_PREFIX: "MYTEAM"
```

### Custom Usage

You can modify most things about the job using the variables above. More can be added if required. You can also use Gitlab syntax to overide when and how it runs too.

#### Example: Your Naming Sucks - I want to choose my own

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/software-composition/blackduck/sca-blackduck.yml'
sca-blackduck:
  variables:
    BD_API_KEY: $BLACKDUCK_API_KEY
    BD_PROJECT_PREFIX: "MYTEAM"
    BD_PROJECT_NAME: "Super-Project"
```

#### Example: Run Only in MASTER branch and Do Not Break the Build

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/software-composition/blackduck/sca-blackduck.yml'
sca-blackduck:
  variables:
    BD_API_KEY: $BLACKDUCK_API_KEY
    BD_PROJECT_PREFIX: "MYTEAM"
    BD_WAIT_FOR_RESULTS: false
# Override template
sca-blackduck:
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      when: always
```

#### Example: Pass some extra flags for my Maven build

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/software-composition/blackduck/sca-blackduck.yml'
sca-blackduck:
  variables:
    BD_API_KEY: $BLACKDUCK_API_KEY
    BD_PROJECT_PREFIX: MYTEAM
    BD_EXTRA_FLAGS: '--detect.maven.build.command="-s $CI_PROJECT_DIR/maven-settings.xml" --detect.maven.path="$CI_PROJECT_DIR/mvnw"'
```
