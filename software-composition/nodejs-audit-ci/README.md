# AUDIT-CI FOR NODEJS

[[_TOC_]]

## Description

Provides a audit of your npm/audit dependencies and packages and fails on vulnerabilities of severity you choose.

This job expects a config file in your project `audit-ci.example.json` or check the documention at https://www.npmjs.com/package/audit-ci

## Usage

### Available Variables

| Name | Default Value | Notes |
| ---  | ---           | --- |
| AUDIT_CI_CONFIG_FILE | `"cicd/sca-audit-ci/audit-ci-config.json"` | Change if your config is under another path |

### Default Usage 

The default case is simply to use the template as it is if your config file is under `cicd/sca-audit-ci/audit-ci-config.json`

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/software-composition/nodejs-audit-ci/sca-audit-ci.yml'
```

### Custom Usage

Most configuration is done via config file. If you want to change the path to file in your project use:

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/software-composition/nodejs-audit-ci/sca-audit-ci.yml'
sca-audit-ci:
  variables:
    AUDIT_CI_CONFIG_FILE: "/completely/different/config.json"
```
