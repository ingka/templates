# Renovate

[[_TOC_]]

## What is Renovate?

Renovate provides automatic Merge Requests to keep your software dependencies up to date! (https://github.com/renovatebot/renovate/blob/master/readme.md)

It supports almost all common language package managers such as npm, maven, pip, gomod, etc, Plus Dockerfiles, K8s manifests, Terraform even your Gitlab CI files. For a full list see https://docs.renovatebot.com/modules/manager/

## How does it work?

Note that this is not a template, it is an example for common configuration because you will likely want to customise the details yourself. But the essentials are:

1. Create a project for Renovate
2. It will run on a schedule of your choosing
3. Each execution will process your projects and creates merge requests in those projects for out of date packages and dependencies
4. You can Merge the merge request (approve), Close the Merge request (ignore this version of the package update), or take other actions such as rebasing or adding to the changes before merge.

## First Run

The first time Renovate runs for a specific project, it will create a suggested configuration for renovate and a preview of what Merge Requests it would have created if it was active.

You can of course modify that suggested configuration before merging. Once the renovate config is merged the project becomes active and Renovate will start providing the dependency updates.

## Suggested Setup

Below is a suggested setup - it just captures the likely scenario plus how to enable usage of artifactory for private dependencies.

You can also refer to the official docs for customisation and explanations

* https://gitlab.com/renovate-bot/renovate-runner
* https://docs.renovatebot.com/configuration-options/
* https://docs.renovatebot.com/self-hosted-configuration/

### 1. Create project

It is recommended to run Renovate as a seperate Gitlab project essentially just containing it's configuration. Create a new Gitlab project in an appropriate place "e.g. `Renovate Bot`"

### 2. Create CICD Variables

You will need a minimum of two CICD Variables

* `RENOVATE_TOKEN` : Containing a Gitlab PAT for the Renovate Bot to use when analysing projects and creating merge requests. Suggest that this is a service account or similar that is a member of the group / projects you want to analyse. It will need the scopes: `read_user`, `api` and `write_repository`
* `GITHUB_COM_TOKEN` : This is any user PAT from github.com - it needs no scopes and exists only to act as a real github user so that it does not get rate-limited in it's many requests

If you need Renovate to also access dependencies in Artifactory then also add:

* `ARTIFACTORY_JWT` : An Artifactory access token (essentially a long lived JWT) with the appropriate permissions.

### 3. Create the Renovate Pipeline

Renovate provide a template for Gitlab and it can be simply added to your project as `.gitlab-ci.yml`

```yaml
include:
  - project: 'renovate-bot/renovate-runner'
    file: '/templates/renovate.gitlab-ci.yml'

renovate:
  variables:
    #RENOVATE_EXTRA_FLAGS: --autodiscover=true --autodiscover-filter=some-group-name/*
    RENOVATE_EXTRA_FLAGS: group1/projectB group2/projectA group2/projectX
    RENOVATE_ONBOARDING: 'true'
    RENOVATE_ONBOARDING_CONFIG: '{"$$schema": "https://docs.renovatebot.com/renovate-schema.json", "extends": ["config:base", ":semanticCommits"], "branchPrefix": "rv8/", "prConcurrentLimit": 3, "branchConcurrentLimit": 3, "hashedBranchLength": 20 }'
    RENOVATE_CONFIG_FILE: $CI_PROJECT_DIR/config.js
    # LOG_LEVEL: debug
```

Here follows a description of what each of those means:

* `RENOVATE_EXTRA_FLAGS` : The list of projects you wish Renovate to work with. If you want instead to autodiscover based on the projects that the PAT has access too you can instead use something like the commented out line above
* `RENOVATE_ONBOARDING` : For each project, first create a Merge Request with a suggested configuration based on `RENOVATE_ONBOARDING_CONFIG`. This allows the members of that project to tweak and adjust the settings before implementing for real (see https://docs.renovatebot.com/configuration-options/)
* `RENOVATE_CONFIG_FILE` : (OPTIONAL - SEE BELOW) a path to the config for the Renovate engine itself. Useful for setting global configurations such as auth in Artifactory for example.
* `LOG_LEVEL` : Uncomment to get debug logging

### 4. Optional: Provide Artifactory Authentication

In order to enable authentication for Artifactory you can add a config.js to the Renovate project and use the `RENOVATE_CONFIG_FILE` to point to it.

Here is an example then of the config.js you would create:

```js
module.exports = {
  // enables usage of environment variables
  trustLevel: "high",
  // Map a hostname to the credential it should use
  hostRules: [
    {
      hostName: "artifactory.mycompany.com",
      token: process.env.ARTIFACTORY_JWT
    }
  ]
};
```

### 5. Schedule Renovate!

The last thing to do is now to set Renovate to run on a schedule of your choosing!

* In the Gitlab UI, navigate to your `renovate-bot`project -> CICD -> Schedules
* Name and schedule as required based on master
* To run the first time without waiting for the schedule : CICD -> Schedules -> Click the Play button next to your pipeline
