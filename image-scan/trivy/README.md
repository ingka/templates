# TRIVY IMAGE SCANNER

[[_TOC_]]

## DESCRIPTION

Trivy https://github.com/aquasecurity/trivy is an excellent and lightweight scanner for vulnerabilities in Docker images. It was built for execution in a pipeline to give fast feedback about vulnerabilities in not only the OS and related packages, but also dependencies for some languages' package manager's too (pip, npm/yarn, compose, cargo, gem)

Trivy also provides templating that integrates findings with Gitlab's UI.

## Usage

### Available Variables

| Name | Default Value |
| ---  | ---           |
| REGISTRY | `"registry.gitlab.com"` |
| USERNAME | `$CI_REGISTRY_USER` |
| PASSWORD | `$CI_REGISTRY_PASSWORD` |
| IMAGE_NAME | `$CI_PROJECT_PATH` |
| IMAGE_TAG | `$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA` |
| FAIL_ON | `"MEDIUM,HIGH,CRITICAL"` |
| IGNORE_FILE | `"$CI_PROJECT_DIR/.trivyignore"` |

### Images in Gitlab Image Registry

**Default usage**

If you build your image based based on the naming convention of `registry.gitlab.com/your-group/your-project:branch-SHORT_COMMIT_SHA` then no configuration should be required.

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/image-scan/trivy/image-scan.yml'
```

**Custom Usage**

If you build your image according to another image naming convention or wish to amend other details then you will need provide the appropriate variables.

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/image-scan/trivy/image-scan.yml'
image-scan:
  variables:
    IMAGE_NAME: "some-other-name"
    IMAGE_TAG: "different-tag"
```

### Images in GCR

If the image you wish to scan is pushed to GCR then you can simply update the credentials and path as so:

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/image-scan/trivy/image-scan.yml'
image-scan:
  variables:
    REGISTRY: eu.gcr.io
    USERNAME: _json_key
    PASSWORD: $MY_CICD_VARIABLE_CONTAINING_SA_JSON
    IMAGE_NAME: "some-other-name"
    IMAGE_TAG: "different-tag"
```